import caminte from 'caminte';

const Schema = caminte.Schema,
    config = {
        driver: "mysql",    // or mariadb
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME,
        pool: true // optional for use pool directly
    };

const schema = new Schema(config.driver, config);

export default schema;