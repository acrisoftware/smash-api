import schema from '..';

const User = schema.define('User', {
    name: schema.String,
    surname: schema.String,
    fiscalCode: {type: schema.String, unique: true},
    email: {type: schema.String, unique: true},
    password: schema.String,
    approved: schema.Boolean,
    joinedAt: {type: schema.Date, default: Date.now},
    birthday: schema.Date
});

export default User;